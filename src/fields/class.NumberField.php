<?php

/**
 * class NumberField
 *
 * Create a field
 *
 * @author Marien den Besten
 * @package FormHandler
 * @subpackage Fields
 */
class NumberField extends Field
{
    private $min;
    private $max;
    private $step;

    /**
     * Set minimum value
     *
     * @param integer $min
     * @return NumberField
     * @author Marien den Besten
     */
    public function setMin($min)
    {
        $this->min = $min;
        return $this;
    }

    /**
     * Set maximum
     *
     * @param integer $max
     * @return NumberField
     * @author Marien den Besten
     */
    public function setMax($max)
    {
        $this->max = $max;
        return $this;
    }

    /**
     * Set the allowed steps
     *
     * @param integer $step
     * @return NumberField
     * @author Marien den Besten
     */
    public function setStep($step)
    {
        $this->step = $step;
        return $this;
    }

    /**
     * TextField::getField()
     *
     * Return the HTML of the field
     *
     * @return string the html
     * @author Marien den Besten
     */
    public function getField()
    {
        // view mode enabled ?
        if($this->getViewMode())
        {
            // get the view value..
            return $this->_getViewValue();
        }

        return sprintf(
            '<input type="number" name="%s" id="%1$s" value="%s" %s' . FH_XHTML_CLOSE . '>%s',
            $this->name,
            htmlspecialchars($this->getValue()),
            (!is_null($this->min) ? 'min="' . $this->min . '" ' : '') .
                (!is_null($this->max) ? 'max="' . $this->max . '" ' : '') .
                (!is_null($this->step) ? 'step="' . $this->step . '" ' : '') .
                (isset($this->tab_index) ? 'tabindex="' . $this->tab_index . '" ' : '') .
                (isset($this->extra) ? ' ' . $this->extra . ' ' : '')
                . ($this->getDisabled() && !$this->getDisabledInExtra() ? 'disabled="disabled" ' : ''),
            (isset($this->extra_after) ? $this->extra_after : '')
        );
    }
}