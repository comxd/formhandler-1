<?php

/*
 * Document
 *
 * @author marien
 * @copyright ColorBase™
 */


include '../src/FormHandler.php';

$form = new FormHandler();

CheckBox::set($form, '', 'inherit')
    ->setOptions(array(1 => 'Inherit from parent, integer value'));

CheckBox::set($form, '', 'inherit1')
    ->setOptions(array(1 => 'Linked to above'));

CheckBox::set($form, '', 'inherit3')
    ->setOptions(array('1' => 'Linked to first checkbox, string value'))
    ->setDisabled();

CheckBox::set($form, 'Multi values<br>', 'inherit4')
    ->setOptions(array(
        1 => 'Value 1',
        2 => 'Value 2',
        3 => 'Value 3',
    ))
    ->setValue(2, true, true);

TextField::set($form, 'Linked to first checkbox', 'inherit2');

$link = function($v)
{
    $value = !empty($v);
    return FormHandler::returnDynamic(array($value), null, null, null, 'checkbox');
};

$form->link('inherit', 'inherit1', $link);
$form->link('inherit', 'inherit3', $link);
$form->link('inherit', 'inherit4', $link);

$form->link('inherit', 'inherit2', function($v)
{
    return FormHandler::returnDynamic(json_encode($v), null, null, null, 'text');
});

$var = $form->flush(true);

echo 'Test for linking checkboxes';

echo '<hr><script type="text/javascript" src="//code.jquery.com/jquery-1.11.1.min.js"></script>';

echo $var;