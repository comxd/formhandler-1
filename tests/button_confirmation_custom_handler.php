<?php

/**
 * @author Ruben de Vos <ruben@color-base.com>
 * @copyright ColorBase™
 */

include '../src/FormHandler.php';

$form = new FormHandler();

$form->addLine('<br>Button with custom confirmation handler', true);
Button::set($form, 'Click me!', 'btn_4')
    ->setConfirmation('Do you really want to click this button?')
    ->setConfirmationDescription('And some extra description')
    ->setExtra('onclick="alert(\'Custom handler successfully executed\')"');

$form->addHTML('<div id="testResult"></div>');

$var = $form->flush(true);

echo 'Test for custom button confirmation handler';

echo '<hr><script type="text/javascript" src="//code.jquery.com/jquery-1.11.1.min.js"></script>';

echo '<script>
    $(document).ready(function()
    {
        var div = $("#testResult"),
        confirm = function(message, description, callable, options)
        {
            if(div.hasClass(\'active\'))
            {
                return;
            }

            div.addClass(\'active\');

            var buttonYes = $("<button id=\'buttonYes\' data-value=\'yes\'>Yes</button>"),
                buttonNo = $("<button id=\'buttonNo\' data-value=\'no\'>No</button>");

            div.append("<br><strong>Custom handler</strong><br><br>Message: " + message);

            if(description !== undefined)
            {
                div.append("<br>Description: " + description);
            }

            buttonYes.on(\'click\', function(event)
            {
                callable(options);
            });

            $(document).on(\'click\', \'#buttonYes, #buttonNo\', function(event)
            {
                div.empty().removeClass(\'active\');

                event.stopPropagation();
                return false;
            });


            div.append("<br><br>").append(buttonYes).append(" ").append(buttonNo);
        };

        (function(FormHandler,$,undefined)
        {
            FormHandler.config = {confirmationHandler: confirm};
        }(window.FormHandler = window.FormHandler || {}, $));
    });
</script>';

echo $var;