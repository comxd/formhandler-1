<?php

/*
 * Document
 *
 * @author marien
 * @copyright ColorBase™
 */

ob_start();

include '../src/FormHandler.php';

$form = new FormHandler();

for($i = 1; $i <= 15; $i++)
{
    TextField::set($form, 'Field '. $i, 'field_'. $i);
}

TextField::set($form, 'Insert before 5', 'before_five')
    ->insertBefore('field_5');

TextField::set($form, 'Insert after 6', 'after_six')
    ->insertAfter('field_6');

TextField::set($form, 'Move before 1', 'before_1');

$form->moveFieldBefore('field_1', 'before_1');

TextField::set($form, 'Move after 15', 'after_15');

$form->moveFieldAfter('field_15', 'after_15');

$form->flush();